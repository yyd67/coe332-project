### COE322 Software Engineering and Design Project Repository

This repository contains all the source code and documentation 
for Fall 2018 COE332 Software and Engineering and Design's 
semester project.

Yakov Dyadkin is the owner of the repository and author of the
code.
