from flask import Flask, jsonify, request
import json
import redis
import uuid
import Jobs
import HW1
from hotqueue import HotQueue
import time
import matplotlib.pyplot as plt

#rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
#q=HotQueue("queue",host='172.17.0.1', port=6379, db=1)

rd = redis.StrictRedis(host=Jobs.get_redis_ip(), port=6379, db=0)
q=HotQueue("queue", host=Jobs.get_redis_ip(), port=6379, db=1) 
plots = redis.StrictRedis(host=Jobs.get_redis_ip(), port=6379, db=2)

def make_plot(x,y):
    plt.scatter(x,y,20,'r')
    #ax = plt.gca()
    #ax.get_xaxis().get_major_formatter().set_useOffset(False)
    plt.title('Scatter Plot of Sunspots for Given Years')
    plt.xlabel('Years')
    plt.ylabel('Sunspots')
    plt.savefig('/tmp/scatter_plot.png', dpi=150)


def save_plot_to_redis(key):
    file_bytes = open('/tmp/scatter_plot.png', 'rb').read()
    plots.set(key, file_bytes)

time.sleep(15)
@q.worker
def execute_job(key):
    time.sleep(15)
    job_dic = Jobs.convert_job_fields(key)
    job_dic['status'] = 'In Progress'
    job_dic['last update time'] = Jobs.current_time()
    rd.hmset(key,job_dic)
    
    
    if (job_dic['start']!='None') and (job_dic['end']!='None'):
        data_list = HW1.search_start_end(int(job_dic['start']),int(job_dic['end']))
    
    elif job_dic['start']!='None':
        data_list = HW1.search_start_end(int(job_dic['start']),0)
    
    elif job_dic['end']!='None':
        data_list = HW1.search_start_end(0,int(job_dic['end']))
    
    elif (job_dic['offset']!='None') and (job_dic['limit']!='None'):
        data_list = HW1.search_offset_limit(int(job_dic['offset']),int(job_dic['limit']))
    
    elif job_dic['offset']!='None':
        data_list = HW1.search_offset_limit(int(job_dic['offset']),0)
    else:
        data_list = HW1.search_offset_limit(0,int(job_dic['limit']))

    
    years = []
    sunspots = []

    for data_dic in data_list:
        years.append(data_dic['year'])
        sunspots.append(data_dic['sunspots'])

    
    make_plot(years,sunspots)
    save_plot_to_redis(key)

    time.sleep(15)    
    job_dic['status'] = 'Complete'
    job_dic['last update time'] = Jobs.current_time()
    rd.hmset(key,job_dic)

execute_job()
