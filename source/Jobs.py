import os
import datetime
from datetime import timedelta
import uuid
import redis
import json
from hotqueue import HotQueue

def get_redis_ip():
    return os.environ.get('REDIS_IP')

rd = redis.StrictRedis(host=get_redis_ip(), port=6379, db=0)
q = HotQueue("queue", host=get_redis_ip(), port=6379, db=1)
plots = redis.StrictRedis(host=get_redis_ip(), port=6379, db=2)

#rd = redis.StrictRedis(host='10.0.2.20', port=6379, db=0)
#q=HotQueue("queue",host='10.0.2.20',port=6379, db=1)

def current_time():
    """Returns the current time with UTC timezone offset for US Central"""
    d = timedelta(hours = -6)
    tz = datetime.timezone(d)
    return str(datetime.datetime.now(tz))


def instantiate_job(jid, status, create_time, start, end, offset, limit):
    """Creates a Python object representing a job."""
    return { 'id': jid,
             'status': status,
             'start': start,
             'end': end,
             'offset': offset,
             'limit': limit,
             'create time': create_time,
             'last update time': create_time }


def convert_job_fields(key):
    return { 'id': rd.hget(key,'id').decode('utf-8'),
             'status': rd.hget(key,'status').decode('utf-8'),
             'start': rd.hget(key,'start').decode('utf-8'),
             'end': rd.hget(key,'end').decode('utf-8'),
             'offset': rd.hget(key,'offset').decode('utf-8'),
             'limit': rd.hget(key,'limit').decode('utf-8'),
             'create time': rd.hget(key,'create time').decode('utf-8'),
             'last update time': rd.hget(key,'last update time').decode('utf-8') }


def generate_jid():
    return str(uuid.uuid4())


def generate_job_key(jid):
    return 'job.{}'.format(jid)

def queue_job(jid):
    q.put(generate_job_key(jid))

def add_job(jid, status, start=None, end=None, offset=None, limit=None):
    job_dict = instantiate_job(jid, status, current_time(), start, end, offset, limit)
    rd.hmset(generate_job_key(jid), job_dict)
    #rd.set(generate_job_key(jid),json.dumps(job_dict))
    queue_job(jid)
    return job_dict


def get_job_by_id(jid):
    dic = convert_job_fields(generate_job_key(jid))
    return dic

def get_job_plot_from_db(jid):
    plot = plots.get(generate_job_key(jid))
    return plot

def get_jobs():
    job_list = []
    for key in rd.keys():
        dic = convert_job_fields(key)
        job_list.append(dic)     
    return job_list


