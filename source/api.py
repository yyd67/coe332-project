import HW1
import Jobs
from flask import Flask, jsonify, request, send_file
import requests
import json
import io

bstream = io.BytesIO(b'some_bytes')

app = Flask(__name__)

def get_data():
    return HW1.make_dictionaries()

DATA = get_data()

@app.route('/sunspots', methods=['GET'])
def get_sunspots():
    offset = request.args.get('offset')
    limit = request.args.get('limit')
    start = request.args.get('start')
    end = request.args.get('end')
  
    if (start and limit) or (start and offset) or (limit and end) or (offset and end):
        return jsonify('Limit and/or offset cannot be combined with start and/or end.'), 400
    
    if offset:
        try:
            offset = int(offset)
        except:
            return jsonify('Limit and offset, if provided, must be integers.'), 400
    if limit:
        try:
            limit = int(limit)
        except:
            return jsonify('Limit and offset, if provided, must be integers.'), 400
   
    
    if start:
        try:
            start = int(start)
        except:
            return jsonify('Start and end, if provided, must be integers.'), 400
    if end:
        try:
            end = int(end)
        except:
            return jsonify('Start and end, if provided, must be integers.'), 400

   
    if (start and end):
        return jsonify( HW1.search_start_end(int(start),int(end)))
    elif start:
        return jsonify(HW1.search_start_end(int(start),0))
    elif end:
        return jsonify(HW1.search_start_end(0,int(end)))
    elif (offset and limit):
        return jsonify(DATA[offset:offset+limit])
    elif offset:
        return jsonify(DATA[offset:])
    elif limit:
        return jsonify(DATA[:limit])
    else:
        return jsonify(DATA)



@app.route('/sunspots/<id_s>', methods=['GET'])
def get_sunspots_id(id_s):
    try:
        id_s = int(id_s)
    except:
        return jsonify('Invalid value provided for row id.'), 400       
    
    return jsonify(DATA[id_s])



@app.route('/sunspots/year/<year>', methods=['GET'])
def get_sunspots_year(year):
    try:
        year = int(year)
    except:
        return jsonify('invalid vlaue provided for year.'), 400
    
    for dictionary in DATA:
        if dictionary['year'] == year:
            return jsonify(dictionary)   
    return jsonify('Invalid vlaue provided for year.'), 400
    


@app.route('/sunspots/jobs', methods=['GET', 'POST'])
def jobs():
    """List all jobs and create new jobs."""
    if request.method == 'POST':
        try:
            job = request.get_json(force=True)
        except Exception as e:
            return json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
        #access job inputs just like any other dictionary -- validate any required fields for type, etc.
        
        if (job.get('start') and job.get('limit')) or (job.get('start') and job.get('offset'))\
        or (job.get('limit') and job.get('end')) or (job.get('offset') and job.get('end')):
            return json.dumps({'status': "Error", 'message': 'Limit and/or offset cannot be combined with start and/or end.'})

        if job.get('start'):
            start = job.get('start')
            try:
                int(start)
            except:
                return json.dumps({'status': "Error", 'message': 'start parameter required and must be an integer.'})
        
        if job.get('end'):
            end = job.get('end')
            try:
                int(end)
            except:
                return json.dumps({'status': "Error", 'message': 'end parameter required and must be an integer.'})
            
            #result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', start=job['start'], end=job['end'])

        if job.get('offset'):
            offset = job.get('offset')
            try:
                int(offset)
            except:
                return json.dumps({'status': "Error", 'message': 'offset parameter required and must be an integer.'})

        if job.get('limit'):
            limit = job.get('limit')
            try:
                int(limit)
            except:
                return json.dumps({'status': "Error", 'message': 'limit parameter required and must be an integer.'})

            #result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', offset=job['offset'], limit=job['limit'])

        if (job.get('start') and job.get('end')):
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', start=job['start'], end=job['end'])
        elif job.get('start'):
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', start=job['start'])
        elif job.get('end'):
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', end = job['end'])
        elif (job.get('offset') and job.get('limit')):
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', offset=job['offset'], limit=job['limit'])
        elif job.get('offset'):
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', offset=job['offset'])
        else:
            result = Jobs.add_job(Jobs.generate_jid(), 'SUBMITTED', limit=job['limit'])

        # once we have a valid job, we need to store it in the database:
        return jsonify({'msg': 'Job created successfully.', 'result': result})

    if request.method == 'GET':
        return jsonify(Jobs.get_jobs())


@app.route('/sunspots/jobs/<job_id>', methods=['GET'])
def get_job_by_id(job_id):
    try: 
        dictionary = Jobs.get_job_by_id(str(job_id))
    except:
        return jsonify('Invalid id or non existant id provided for job.'), 400
    #dictionary = Jobs.get_job_by_id(str(job_id))
    return jsonify(dictionary)

@app.route('/sunspots/jobs/<job_id>/plot', methods=['GET'])
def get_job_plot(job_id):

    # do something to get the plot from the database:
    plot = Jobs.get_job_plot_from_db(job_id)
    return send_file(io.BytesIO(plot),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format(job_id))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

