def make_dictionaries(): 
  data_list = []
  id = 0
  with open('sunspots.csv', 'r') as time_series:
    for row in time_series:
      line1 = row.strip('\n')
      line2 = line1.split(',')
      sunspots = dict([('id',id), ('year',int(line2[0])), ('sunspots',int(line2[1]))])  
      id += 1
      data_list.append(sunspots)
  return data_list


def search_start_end(start=0, end=0):
  sunspots = make_dictionaries() 
  
  if (start == 0 and end == 0 ):
    return sunspots
  
  elif start == 0:
    index = 0
    while((sunspots[index])['year'] < end ):
      index+=1
    return sunspots[0:index+1]

  elif end == 0:
    index = 0
    while((sunspots[index])['year'] < start):
      index+=1
    return sunspots[0:index+1]
  
  else:
    start_index = 0
    end_index = 0 
    for entry in sunspots:
      if entry['year'] == start:
        start_index = entry['id']
      if entry['year'] == end:
        end_index = entry['id']
    return sunspots[start_index:end_index+1]


def start_end_input():
  start = int(input('Enter starting year between 1770 to 1869, or press enter for no year: ') or '0' )

  while (start < 1770 or start > 1869):
    if start == 0:
      end = int(input('Enter end year between 1770 to 1869, or press enter for no year: ') or '0')
      while end < 1770 or end > 1869:
        if end == 0:
          return search_start_end(start,end)
        else:
          print('\nEnter valid end year')
          end = int(input('Enter end year between 1770 to 1869, or press enter for no year: ') or '0')
      return search_start_end(start,end)
    print('\nEnter valid starting year')
    start = int(input('Enter starting year between 1770 to 1869, or press enter for no year: ') or '0')

  end = int(input('Enter end year between 1770 to 1869, or press enter for no year: ') or '0')
  while end < 1770 or end > 1869:
    if end == 0:
      return search_start_end(start,end)
    else:
      print('\nEnter valid end year')
      end = int(input('Enter end year between 1770 to 1869, or press enter for no year: ') or '0')

  return search_start_end(start, end)


def search_offset_limit(offset=0, limit=0):
  sunspots = make_dictionaries()
  length = len(sunspots)
   
  assert (offset + limit) < 100  

  if (offset == 0 and limit == 0 ):
    return sunspots

  elif offset == 0:
    return sunspots[:limit]

  elif limit == 0:
    return sunspots[offset:]

  else:
    return sunspots[offset:offset+limit]


def main():
  query = input('Select query. a) whole data set; b) search by start and end year; c) search by limit and offset: ' )

  while( (query != 'a') and (query !='b') and (query !='c') ):
    print('Please enter a valid query option')
    query = input('Select query. a) whole data set; b) search by start and end year; c) search by limit and offset: ' ) 

  if query == 'a':
    print(make_dictionaries())

  elif query == 'b':
    print(start_end_input())

  elif query == 'c': 
    offset = int(input('Enter search offset between 1 to 99, or 0 for no offset: '))
    limit = int(input('Enter search limit between 1 to 99, or 0 for no limit: '))

    print(search_offset_limit(offset, limit))
