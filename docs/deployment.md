### Getting the Api Up and Running
This little tutroial will detail how to launch the api for two different set ups:  
* Singualr virtual machine  
* Two virtual machine
  
  
   
#### Pulling the Project Repository
The first in getting the api to run on any set up is to clone the project repositoty 
which can easily be done with the following command in any git equipped terminal:
``` 
git clone https://yyd67@bitbucket.org/yyd67/coe332-project.git
```
  
  
  
#### Downlading Docker Hub Images
Next the docker images for the api and the worker need to be downloaded from Docker 
Hub in order to create the api and worker containers; detailed in the next section. 
Assuming the user already has docker set up on his machine/s this can be done by 
implementing the following commands:
  
for the api image    
```docker pull yyd67/api```
  
and for the worker image  
```docker pull yyd67/worker```
  
  
  
#### Launching Api and Worker Container
Launching the api server and the worker service is done by constructing containers 
from the images previously pulled from Docker Hub and using the docker-compose.yml 
files in the repository. The following subections explain how to do so for the a 
single vm (virtual machine) and for two vms.   
  
##### Singular VM
The simplest case for running the api would be to have the api server, the worker service,
and the redis database running on the same machine. Doing so requires the user to use two
docker-compose commnads to deploy the api server, the worker service, and the redis 
databse conatiners, irespective of order.  
  
Launching the api container:  
```docker-compose -f docker-compose-local.yml up -d```  
this command specifies the docker-compose filename with the '-f' flag and tells to put 
the container in the background 'daemonizng' it with the 'd' flag. The container can be
launched it the foreground without the '-d' flag.
  
Launching the worker service:
The *docker-compose-worker.yml* file creates two containers runnning the worker and the
redis database respectively that can be created with  
```docker-compose -f docker-compose-worker.yml up -d```  
  
  
##### Two VMs
For two machines the api server will run on one machine and the worker and redis
database will run on another machine.   
Launching the worker and redis is done exactly the same as in the single vm case:  
```docker-compose -f docker-compose-worker.yml up -d```  
  
Launching the api container on the other machine is slightly different than in the
previous case. Before creating the container with a new docker-compose file, in this 
case *docker-compose.yml*, the ip address `REDIS_IP` specified in the file needs to 
be changed to the private ip adress of the machine where the worker and redis 
containers are running.  
```python
version: '3'
services:
  web:
    image: yyd67/api
    ports:
      - "5000:5000"
    environment:
      REDIS_IP: 10.0.2.20
```  
Now we can create the api container with  
```docker-compose up -d```
  
  
   
After these steps the user should be able to interact with server.  
:)












