### USER GUIDE and Minor Documentation  
  
The api utilizes RESTful architecture *Flask python Library* and Redis database to operate. 
Users can access the data and perform analysis on it using http requests.   

 
#### Documentation
* 4 Python modules  
    * *HW1.py* code that converts the time series csv file to a python list of dictionaries.  
    * *api.py* code that creates the api server.  
    * *Jobs.py* code that creates and interacts with jobs.  
    * *worker.py* code that performs analysis on jobs and interacts with job queue.  
* *sunpspots.csv* contains the time series data of the number of sunspots per year
  from 1770 to 1869.    
* 2 Dockerfiles  
    * *Dockerfile* file that is used to create the api docker image
    * *Dockerfile-worker* file that is used to create the worker docker image*  
* *requirements.txt* list of libraries used for the docker images.  
  

#### EndPoints and User Guide
The endpoints of the api are as follow:  

#### ```'BASE_URL/sunspots', methods=['GET'] ```      
Returns the time series or selections from the series based on four parameters; start, end, limit, 
and offest that the user can input in the http request. Start and end correspond to a starting and 
ending year for a segment of the time series, while limit and offset correspond to the search strating 
index offest for data point/s and limit to the amount of desired data points in a segment.  


pyhton  
```Python
import requests
rsp = requests.get("http://localhost:5000/sunspots?start=1771&end=1773")
```

Curl  
```
curl "http://localhost:5000/sunspots?start=1771&end=1773" 
```  
  
  
Sample output:    
``` 
  {
    "id": 1,
    "sunspots": 82,
    "year": 1771
  },
  {
    "id": 2,
    "sunspots": 67,
    "year": 1772
  },
  {
    "id": 3,
    "sunspots": 35,
    "year": 1773
  }
```  

#### ```'BASE_URL/sunspots/<id_s>', methods=['GET']```  

Returns the data point corresponding to the user input id for the point. Since there are 100 points in
time series the id number go from 0 to 99. 

Python  
```python  
import requests
rsp = requests.get("http://localhost:5000/sunspots/1")
```  

Curl
```  
curl "http://localhost:5000/sunspots/1"
```
Sample output:  
```
{
  "id": 1,
  "sunspots": 82,
  "year": 1771
}
```  
  
    
#### ```'BASE_URL/sunspots/year/< year >', methods=['GET']```  

Returns the data point corresponding to the user requested year associated with the data point.  
  
Python  
```  
import requests
rsp = requests.get("http://localhost:5000/sunspots/year/1771")
```  

Curl  
```
curl "http://localhost:5000/sunspots/year/1771"
```
Sample output:  
```  
{
  "id": 1,
  "sunspots": 82,
  "year": 1771
}
```  
  
  
  
#### ```'BASE_URL/sunspots/jobs', methods=['GET','POST']```  

Creates a job from a user generated http post request and adds the the job to a redis data base and queue
or returns a list of all current jobs if the user sends a get request. This endpoint uses numerous 
functions from the *Job.py* module to create unique ids for the jobs and store the newly created jobs 
with each job's specific attributes in a redis database, and adds the job id to a queue that the worker
services perfroms analysis on.

Post Jobs:  
curl  
```
curl -d '{"end": 1850}' -H "Content-Type: application/json" -X POST http://10.0.2.39:5000/sunspots/jobs
{
  "msg": "Job created successfully.",
  "result": {
    "create time": "2018-12-14 01:00:14.909670-06:00",
    "end": 1850,
    "id": "03fbdd74-8b32-41bc-8bf4-228fc7d16a70",
    "last update time": "2018-12-14 01:00:14.909670-06:00",
    "limit": null,
    "offset": null,
    "start": null,
    "status": "SUBMITTED"
  }
}
```

View all Jobs:  
curl  
```
ubuntu@yyd67-1:~/coe332/coe332-project/source$ curl http://10.0.2.39:5000/sunspots/jobs
[
  {
    "create time": "2018-12-14 01:00:14.909670-06:00",
    "end": "1850",
    "id": "03fbdd74-8b32-41bc-8bf4-228fc7d16a70",
    "last update time": "2018-12-14 01:00:45.192808-06:00",
    "limit": "None",
    "offset": "None",
    "start": "None",
    "status": "Complete"
  }
]
```
  
  
#### ```'BASE_URL/sunspots/jobs/<job_id>', methods=['GET']```  
  
  
Returns the status of the specific job requsted by the user through the job id.  
  
curl    
```  
ubuntu@yyd67-1:~/coe332/coe332-project/source$ curl http://10.0.2.39:5000/sunspots/jobs/03fbdd74-8b32-41bc-8bf4-228fc7d16a70
{
  "create time": "2018-12-14 01:00:14.909670-06:00",
  "end": "1850",
  "id": "03fbdd74-8b32-41bc-8bf4-228fc7d16a70",
  "last update time": "2018-12-14 01:00:45.192808-06:00",
  "limit": "None",
  "offset": "None",
  "start": "None",
  "status": "Complete"
}
```

#### ```'BASE_URL/sunspots/jobs/<job_id>/plot', methods=['GET']```  

Returns a png image of a scatter plot created from the job parameters. In the follwoing command
the user saved file to a png image called *testplot5*  

curl  
```  
ubuntu@yyd67-1:~/coe332/coe332-project/source$ curl http://10.0.2.39:5000/sunspots/jobs/
03fbdd74-8b32-41bc-8bf4-228fc7d16a70/plot --output testplot6
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 39606    0 39606    0     0  3867k      0 --:--:-- --:--:-- --:--:-- 3867k
```

![Sample Plot](https://bitbucket.org/yyd67/coe332-project/raw/28f6a047d5e1ba5be764d72e46ae2edd129b49fd/source/testplot4.png)  
  
#### Usage Charge  
The use could be charged based on the amount of job requsts sent; that is each job request has an
assocaited operation that it will perorm on the data set and the more memory that is required to
perform that operation the more the user will be charged. Additionally, the user will have limits on
the amount of jobs he/she can request in order to accomodate for other users and a finite amount of
resources.  
:)